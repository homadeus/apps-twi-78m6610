// /* A 24-bit unsigned integer having a range of 0 to 16777215. Typically used for counters */
// #define UINT24 3
// /* A 24-bit signed integer with a range of -8388608 to +8388607. */
// #define INT24 3
// /* A 24-bit unsigned scaled integer (scaled by the user). */
// #define USI24 3
// /* A 24-bit signed scaled integer (scaled by the user). */
// #define SSI24 3
// /* A 24-bit unsigned fixed-point value with the binary point to the left of bit 23 with a range of 0 to 1-2-24. */
// #define U.24 3
// /* A 24-bit unsigned fixed-point number with the binary point to the left of bit 22 and with a range of 0 to 1-2-23. */
// #define U.23 3
// /* A 24-bit unsigned fixed-point number with the binary point to the left of bit 21 and with a range of 0 to 2-2-22. */
// #define U.22 3
// /* A 24-bit unsigned fixed point number with the binary point to the left of bit 20 and with a range of 0 to 4-221. */
// #define U.21 3
// /* A 24-bit signed fixed point number with the binary point to the left of bit 22 and with a range of -1.0 to 1-2-23. */
// #define S.23 3
// /* A 24-bit signed fixed-point number with a binary point to the left of bit 20 and with a range of -4.0 to 4-2-21. */
// #define S.21 3
// /* A 24-bit signed fixed-point number with a binary point to the left of bit 16 and with a range of -128 to +128 */
// #define S16 3
// /* A variable containing 24 independent single-bit values. */
// #define B24 3
//

void maxim_78M6610_debug_version();


uint32_t maxim_78M6610_get_register_uint24(uint8_t reg);
int32_t maxim_78M6610_get_register_int24(uint8_t reg);

uint32_t maxim_78M6610_get_voltage_rms();
uint32_t maxim_78M6610_get_current_rms();
int32_t maxim_78M6610_get_power();
int32_t maxim_78M6610_get_power_average();
uint32_t maxim_78M6610_get_frequency();

int32_t maxim_78M6610_get_temperature_as_signed_int();

void maxim_78M6610_set_temperature_scale( uint8_t[] );
int8_t maxim_78M6610_get_temperature_integer_part();


#define MAXIM_78M6610_REGISTERS \
	X(Harm        , HARM               	  , UINT24 , "Harmonic selector. [Default = 1]"                                                , (0x105/3)) \
	X(Accum       , ACCUM              	  , UINT24 , "Accumulation Interval for calculation (RMS, etc.). [Default = 400]"              , (0x10B/3)) \
	X(Iscale      , I_SCALE               , UINT24 , "Current scaling register"                                                        , (0x11D/3)) \
    X(Vscale      , V_SCALE               , UINT24 , "Voltage scaling register"                                                        , (0x120/3)) \
    X(Pscale      , P_SCALE               , UINT24 , "Power scaling register"                                                          , (0x123/3)) \
    X(PFscale     , PF_SCALE              , UINT24 , "Power Factor scaling register"                                                   , (0x126/3)) \
    X(Fscale 	  , F_SCALE               , UINT24 , "Frequency scaling register"                                              		   , (0x129/3)) \
    X(Tscale      , T_SCALE         	  , UINT24 , "Temperature scaling register"                                                    , (0x12C/3)) \
    X(Command     , COMMAND               , B24    , "Command Register (see Command Register Section)"                                 , (0x000/3)) \
    X(FWversion   , FW_VERSION            , UINT24 , "Firmware release date in hex format (0x00YYMD)"                                  , (0x003/3)) \
    X(Temperature , TEMPERATURE           , S16    , "Chip Temperature (-128°C to +128°C, binary point to left of bit 16)"             , (0x012/3)) \
    X(VA          , VA                    , SSI24  , "Apparent Power (LSB weight determined by Pscale)"                                , (0x015/3)) \
    X(VAR         , VA_REACTIVE           , SSI24  , "Reactive Power (LSB weight determined by Pscale)"                                , (0x018/3)) \
    X(Vrms        , V_RMS                 , USI24  , "RMS Voltage (LSB weight determined by Vscale)"                                   , (0x01B/3)) \
    X(Irms        , I_RMS                 , USI24  , "RMS Current (LSB weight determined by Iscale)"                                   , (0x01E/3)) \
    X(Watt        , P                     , SSI24  , "Active Power (LSB weight determined by Pscale)"                                  , (0x021/3)) \
    X(PAverage    , P_AVG                 , SSI24  , "Active Power Averaged over 30s Window (LSB determined by Pscale)"                , (0x024/3)) \
    X(PF          , POWER_FACTOR          , SSI24  , "Power Factor (LSB weight determined by PFscale)"                                 , (0x027/3)) \
    X(Frequency   , FREQUENCY             , USI24  , "Line Frequency (LSB weight determined by Fscale)"                                , (0x02A/3)) \
    X(Alarms      , ALARMS                , B24    , "Alarm Status Registers"                                                          , (0x030/3)) \
    X(Vfund       , V_FUND                , USI24  , "RMS Voltage (Fundamental) (LSB weight determined by Vscale)"                     , (0x033/3)) \
    X(Ifund       , I_FUND                , USI24  , "RMS Current (Fundamental) (LSB weight determined by Iscale)"                     , (0x036/3)) \
    X(Pfund       , P_FUND                , SSI24  , "Active Power (Fundamental) (LSB weight determined by Pscale)"                    , (0x039/3)) \
    X(Qfund       , Q_FUND                , SSI24  , "Reactive Power (Fundamental) (LSB weight determined by Pscale)"                  , (0x03C/3)) \
    X(VAfund      , VA_FUND               , SSI24  , "Apparent Power (Fundamental) (LSB weight determined by Pscale)"                  , (0x03F/3)) \
    X(Vharm       , V_HARMONIC            , USI24  , "RMS Voltage (Harmonic) (LSB weight determined by Vscale)"                        , (0x042/3)) \
    X(Iharm       , I_HARMONIC            , USI24  , "RMS Current (Harmonic) (LSB weight determined by Iscale)"                        , (0x045/3)) \
    X(Pharm       , P_HARMONIC            , SSI24  , "Active Power (Harmonic) (LSB weight determined by Pscale)"                       , (0x048/3)) \
    X(Qharm       , Q_HARMONIC            , SSI24  , "Reactive Power (Harmonic) (LSB weight determined by Pscale)"                     , (0x04B/3)) \
    X(VAharm      , VA_HARMONIC           , SSI24  , "Apparent Power (Harmonic) (LSB weight determined by Pscale)"                     , (0x04E/3)) \
    X(ILow        , I_LOW                 , USI24  , "Lowest RMS Current Recorded Since Reset (LSB determined by Iscale)"              , (0x057/3)) \
    X(IHigh       , I_HIGH                , USI24  , "Highest RMS Current Recorded Since Reset (LSB determined by Iscale)"             , (0x05A/3)) \
    X(Ipeak       , I_PEAK                , USI24  , "Highest Current in last accumulation Interval (LSB determined by Iscale)"        , (0x05D/3)) \
    X(VLow        , V_LOW                 , USI24  , "Lowest RMS Voltage Recorded Since Reset (LSB determined by Vscale)"              , (0x060/3)) \
    X(VHigh       , V_HIGH                , USI24  , "Highest RMS Voltage Recorded Since Reset (LSB weight determined by Vscale)"      , (0x063/3)) \
    X(Vpeak       , V_PEAK                , USI24  , "Highest Voltage in last accumulation Interval (LSB weight determined by Vscale)" , (0x066/3)) \
    X(ExtTemp1    , EXTERNAL_TEMP_1       , USI24  , "External temperature 1 (ATEMP1 input)"                                           , (0x0D5/3)) \
    X(ExtTemp2    , EXTERNAL_TEMP_2       , USI24  , "External temperature 2 (ATEMP2 input)"                                           , (0x0D8/3)) \
    X(Divisor     , DIVISOR               , UINT24 , "Actual Accumulation interval for low rate results"                               , (0x10E/3)) \
    X(DIOState    , DIO_STATE             , B24    , "State of DIO Outputs"                                                            , (0x147/3)) \
    X(Irms1       , I_RMS_UNSCALED        , UINT24 , "Unscaled RMS Current"                                                            , (0x156/3)) \
    X(Vrms1       , V_RMS_UNSCALED        , UINT24 , "Unscaled RMS Voltage"                                                            , (0x159/3)) \
    X(Power1      , P_RMS_UNSCALED        , INT24  , "Unscaled Active Power"                                                           , (0x15C/3)) \
    X(Var1        , VA_REACTIVE_UNSCALED  , INT24  , "Unscaled Reactive Power"                                                         , (0x15F/3)) \
    X(VA1         , VA_UNSCALED           , INT24  , "Unscaled Apparent Power"                                                         , (0x162/3)) \
    X(PF1         , POWER_FACTOR_UNSCALED , INT24  , "Unscaled Power Factor"                                                           , (0x165/3)) \
    X(Freq1       , FREQUENCY_UNSCALED    , UINT24 , "Unscaled Frequency"                                                              , (0x168/3))


#define MAXIM_78M6610_TWI_ADDR (0x00)

typedef enum {
#define X(name1, name2, type, desc, value) \
    MAXIM_78M6610_##name2 = value,
    MAXIM_78M6610_REGISTERS
#undef X
} maxim_registers;


/*
SCALING REGISTERS

http://www.maximintegrated.com/app-notes/index.mvp/id/5596

		The 78M6610+PSUEVK Default Scaling
			Maximum 	Resolution 				Scaling Value
Voltage 	667V 		0.001V 					667000
Current 	62.5A 		0.0000078125A (1/128mA) 8000000
Power 		41687.5W 	0.005W 					8337500

*/

/* The scaling maximum values are static for a given circuit
 *   dividing this value with scaling value wi obtain the RESOLUTION of the proper registers
 */
#define  MAXIM_78M6610_SCALING_MAXIMUM_VOLTAGE 667
#define  MAXIM_78M6610_SCALING_MAXIMUM_CURRENT 62.5
#define  MAXIM_78M6610_SCALING_MAXIMUM_POWER   41687.5

// HACK: RESOLUTION is Scaling Value dependent but this are the values for the default Scaling Values
#define  MAXIM_78M6610_SCALING_RESOLUTION_VOLTAGE_VOLTS 	  0.001 	  // Volts
#define  MAXIM_78M6610_SCALING_RESOLUTION_CURRENT_AMPERE	  0.0000078125// Amperes
#define  MAXIM_78M6610_SCALING_RESOLUTION_CURRENT_AMPERE_MILI 0.0078125   // mili Amperes
#define  MAXIM_78M6610_SCALING_RESOLUTION_POWER_WATTS	   	  0.005		  // Watts



