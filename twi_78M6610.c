/*
 * Homadeus Platform TWI Driver
 * Copyright (C) 2013 Homadeus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <stdio.h>

#include <twi/twi.h>

#include <homadeus/devices/78M6610.h>

#include <homadeus/utils/utils.h>

/*
void maxim_78M6610_debug_version()
{
  twi_init();
  twi_setAddress(MAXIM_78M6610_TWI_ADDR);
  uint8_t reg = MAXIM_78M6610_FW_VERSION;
  uint8_t data[3];

  twi_writeTo(MAXIM_78M6610_TWI_ADDR, &reg, 1, 1);
  twi_readFrom(MAXIM_78M6610_TWI_ADDR, data, 3);

  printf("%x%x%x\n", data[0], data[1], data[2]);

}*/



uint32_t uint24_to_uint32(const uint8_t *src, uint8_t count)
{
	uint32_t result = 0;

	result |= ( ( (uint32_t)src[0] << (8*2) ) | ( (uint32_t)src[1] << (8*1) ) |   ( (uint32_t)src[2] << (8*0) ) );

	return result;

}

//SSI24
//UINT24
int32_t int24_to_int32(const uint8_t *src, uint8_t count) // Little Endian
{
	int32_t result = 0;

	// Little trick for having the signed msb extended:
	//   http://stackoverflow.com/questions/14721275/how-can-i-use-arithmetic-right-shifting-with-an-unsigned-int?lq=1

	int16_t MSB = ( (int16_t)src[0] << (8) ) >> 8;

	result |= ( (int32_t)MSB << (8*2) )  | ( (uint32_t)src[1] << (8*1) ) | ( (uint32_t)src[2] << (8*0) );

	return result;

}

/*
 *
 * Intput:
 *  reg: register to write to
 *  data: a three-byte array -data to write to the register-.
 */

void maxim_78M6610_write_to_register(uint8_t reg, uint8_t data[3])
{

	int rc = twi_writeTo_register(MAXIM_78M6610_TWI_ADDR, reg , data , 3) ;
	if (rc) { printf(" [error writing to 78M6610: %d] ", rc);  }

}

uint32_t maxim_78M6610_get_register_uint24(uint8_t reg)
{

	uint8_t len = 3;
	uint8_t data[len];

	int rc = twi_writeTo(MAXIM_78M6610_TWI_ADDR, &reg, 1, 1);
	if (rc) { printf(" [error writing to 78M6610: %d] ", rc);  }

	rc = twi_readFrom(MAXIM_78M6610_TWI_ADDR, data, len);

	return  uint24_to_uint32(data, len);

}

int32_t maxim_78M6610_get_register_int24(uint8_t reg)
{

	uint8_t len = 3;
	uint8_t data[len];

	int rc = twi_writeTo(MAXIM_78M6610_TWI_ADDR, &reg, 1, 1);
	if (rc) { printf(" [error writing to 78M6610: %d] ", rc);  }

	rc = twi_readFrom(MAXIM_78M6610_TWI_ADDR, data, len);

	return  int24_to_int32(data, len);

}


uint32_t maxim_78M6610_get_voltage_rms()
{

	uint8_t reg = MAXIM_78M6610_V_RMS;
	uint8_t len = 3;
	uint8_t data[len];

	int rc = twi_writeTo(MAXIM_78M6610_TWI_ADDR, &reg, 1, 1);
	if (rc) { printf(" [error writing to 78M6610: %d] ", rc);  }

	rc = twi_readFrom(MAXIM_78M6610_TWI_ADDR, data, len);

	return  uint24_to_uint32(data, len);

}


uint32_t maxim_78M6610_get_current_rms()
{
	uint8_t reg = MAXIM_78M6610_I_RMS;
	uint8_t len = 3;
	uint8_t data[len];

	int rc = twi_writeTo(MAXIM_78M6610_TWI_ADDR, &reg, 1, 1);
	if (rc) { printf(" [error writing to 78M6610: %d] ", rc);  }

	rc = twi_readFrom(MAXIM_78M6610_TWI_ADDR, data, len);

	return  uint24_to_uint32(data, len);
}

int32_t maxim_78M6610_get_power()
{
	uint8_t reg = MAXIM_78M6610_P;
	uint8_t len = 3;
	uint8_t data[len];

	int rc = twi_writeTo(MAXIM_78M6610_TWI_ADDR, &reg, 1, 1);
	if (rc) { printf(" [error writing to 78M6610: %d] ", rc);  }

	rc = twi_readFrom(MAXIM_78M6610_TWI_ADDR, data, len);

	return  int24_to_int32(data, len);
}

int32_t maxim_78M6610_get_power_average()
{
	uint8_t reg = MAXIM_78M6610_P_AVG;
	uint8_t len = 3;
	uint8_t data[len];

	int rc = twi_writeTo(MAXIM_78M6610_TWI_ADDR, &reg, 1, 1);
	if (rc) { printf(" [error writing to 78M6610: %d] ", rc);  }

	rc = twi_readFrom(MAXIM_78M6610_TWI_ADDR, data, len);

	return  int24_to_int32(data, len);
}

uint32_t maxim_78M6610_get_frequency()
{
	uint8_t reg = MAXIM_78M6610_FREQUENCY;
	uint8_t len = 3;
	uint8_t data[len];

	int rc = twi_writeTo(MAXIM_78M6610_TWI_ADDR, &reg, 1, 1);
	if (rc) { printf(" [error writing to 78M6610: %d] ", rc);  }

	rc = twi_readFrom(MAXIM_78M6610_TWI_ADDR, data, len);

	return  uint24_to_uint32(data, len);
}



int32_t maxim_78M6610_get_temperature_as_signed_int()
{
	uint8_t reg = MAXIM_78M6610_TEMPERATURE;
	uint8_t len = 3;
	uint8_t data[len];

	int rc = twi_writeTo(MAXIM_78M6610_TWI_ADDR, &reg, 1, 1);
	if (rc) { printf(" [error writing to 78M6610: %d] ", rc);  }

	rc = twi_readFrom(MAXIM_78M6610_TWI_ADDR, data, len);

	return  int24_to_int32(data, len);
}





/*
 * Function maxim_78M6610_set_temperature_scale
 * Desc     sets the value of maxim_78M6610 TScale register
 *
 * Input    scale_value: the value to write to TScale. Must be a 24-bit unsigned integer ( values: 0 min to max 8,388,607 (2^23 - 1 )
 *
 *
 * Documentation
 *
 *  From Specs:
 * 		The scaling registers for Line Frequency, Power Factor, and Temperature do not set full scale values.
 * 		They only set resolution. For example, setting Tscale to 1000 the value of the temperature is represented
 * 		in 1/1000 of Degree Celsius. For example, by setting the Tscale register to 1000, a temperature of 27°C
 * 		is reported as 27000 in the Temperature register.
 *
 *  From calibration manual:
 * 		TScale, PFScale, and FScale
 * 		The remaining scaling values — TScale, PFScale, and FScale— do not directly affect calibration or the full-scale
 * 		range of the measurement values, but only determine the resolution, or the LSB weighting, with which the
 * 		value is reported. For example, if FScale is set to 1000, frequency is reported with a resolution of 1/1000Hz.
 * 		Likewise, setting PFScale to 1000 means that the power factor is reported with three digits to the right of the decimal point.
 *
 *
 * */


/*
 *
 * Intput:
 *  scale_value: a three-byte unsigned integer.
 */

void maxim_78M6610_set_temperature_scale(uint8_t scale_value[3])
{

	//WRITE TEMPERATURE SCALE
	uint8_t reg_scale = MAXIM_78M6610_T_SCALE;

	/*uint8_t data_scale[3];

	data_scale[0] = 0x00; //scale_value[0];
	data_scale[1] = 0x00; //scale_value[1];
	data_scale[2] = 0x0A; //scale_value[2];
	*/

	//twi_writeTo_register(MAXIM_78M6610_TWI_ADDR, reg_scale , data_scale , 3) ;

	twi_writeTo_register(MAXIM_78M6610_TWI_ADDR, reg_scale , scale_value , 3) ;

}


// S.16: A 24-bit signed fixed-point number with a binary point to the left of bit 16 and with a range of -128 to +128
// this function returns only the integer part
int8_t s16_to_int8(const uint8_t *src) // Little Endian
{
	return (int8_t)src[0];
}



/*
 * 	!! This function is DEPRECATED and the temperature register should be interpreted as a SSI24,
 * 	use function maxim_78M6610_get_temperature_as_signed_int(). !!
 *
 * 	******************************************************************************************************
 * README!.
 *
 * On the Maxim 78M6610 specification the TEMPRERATURE register is defined as:
 *  * Data Type: S.16.
 *  * Description: Chip Temperature (-128°C to +128°C, binary point to left of bit 16)
 *
 * 	 Data Type "S.16" Description: A 24-bit signed fixed-point number with a binary point to the left of
 * 		bit 16 and with a range of -128 to +128
 *
 * 	However, TEMPERATURE register is scaled by register TScale, from datasheet description:
 * 	  "The scaling registers for Line Frequency, Power Factor, and Temperature do not set full scale values.
 * 	  They only set resolution. For example, setting Tscale to 1000 the value of the temperature is represented
 * 	  in 1/1000 of Degree Celsius. For example, by setting the Tscale register to 1000, a temperature of 27°C
 * 	  is reported as 27000 in the Temperature register."
 *
 * 	Which implies that the Temperature register Data Type might be of type SSI24.
 * 	( SSI24: A 24-bit signed scaled integer (scaled by the user). Range of -8388608 to +8388607. ).
 *
 * 	This assumption is confirmed by empirical experimentation, by setting for instance the value of TScale to 1
 * 	The result given by Temperature register if interpreted as a SSI24 is from the order of magnitude of 50 (Celsius),
 * 	If interpreted as S.16 will be from the order of magnitude < 1 (Celsius).
 *
 * 	******************************************************************************************************
 * 	!! This function is DEPRECATED and the temperature register should be interpreted as a SSI24,
 * 	use function maxim_78M6610_get_temperature_as_signed_int(). !!
 *
 */

int8_t maxim_78M6610_get_temperature_integer_part()
{


	//maxim_78M6610_set_temperature_scale(); // we use this to set the scale for ex. to 1.
	uint8_t scale_value[3]={ 0x00 , 0x00 , 0x01 };
	maxim_78M6610_set_temperature_scale(scale_value);

	// READ TEMPERATURE
	uint8_t reg = MAXIM_78M6610_TEMPERATURE;
	uint8_t len = 3;
	uint8_t data[len];
	int rc = twi_writeTo(MAXIM_78M6610_TWI_ADDR, &reg, 1, 1);
	if (rc) { printf(" [error writing to 78M6610: %d] ", rc);  }

	rc = twi_readFrom(MAXIM_78M6610_TWI_ADDR, data, len);

	return s16_to_int8(data);
}





